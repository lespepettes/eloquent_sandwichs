<?php
/**
 * @apiGroup Categories
 * @apiName GetCategories
 * @apiVersion 0.1.0
 *
 * @api {get} /categories/id  Accès a une ressource catégorie
 *
 * @apiDescription Accès a une ressource de type catégorie
 * permet d'accéder à la représentation d'une ressource categorie .
 * Retourne une représentation json de la ressource, incluant son nom et
 * id.
 *
 *
 * @apiSuccess (Succès : 200) {Number} id Identifiant de la catégorie
 * @apiSuccess (Succès : 200) {String} nom Nom de la catégorie
 * @apiSuccess (Succès : 200) {String} description Description de la catégorie
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *	  {
 *	      
 *    }
 *
*/

/**
 * @apiGroup Categories
 * @apiName GetAllCategories
 * @apiVersion 0.1.0
 *
 * @api {get} /categories/  Accès au ressource catégorie
 *
 * @apiDescription Accès au ressource de type catégorie
 * permet d'accéder à la représentation de toute les ressources categories .
 * Retourne une représentation json de la ressource, incluant son nom et
 * id.
 *
 *
 * @apiSuccess (Succès : 200) {Number} id Identifiant de la catégorie
 * @apiSuccess (Succès : 200) {String} nom Nom de la catégorie
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *	   {
 *	      "nb": 5,
 *		  "categories": [ 
 *            {
 *              "id": 1,
 *              "nom": "salades"
 *            },
 *            {
 *              "id": 2,
 *              "nom": "crudités"
 *            },
 *            {
 *              "id": 3,
 *              "nom": "viandes"
 *            },
 *            {
 *              "id": 4,
 *              "nom": "Fromages"
 *            },
 *            {
 *              "id": 5,
 *              "nom": "Sauces"
 *            }
 *       ]
 *    }
 *
*/

/**
 * @apiGroup Categories
 * @apiName GetCategorie
 * @apiVersion 0.1.0
 *
 * @api {get} /categories/id  Accès à une ressource catégorie
 *
 * @apiDescription Accès à une ressource de type catégorie
 * permet d'accéder à la représentation de la ressource categorie désignée.
 * Retourne une représentation json de la ressource, incluant son nom et
 * sa description.
 *
 * Le résultat inclut un lien pour accéder à la liste des ingrédients de cette catégorie.
 *
 * @apiParam {Number} id Identifiant unique de la catégorie
 *
 *
 * @apiSuccess (Succès : 200) {Number} id Identifiant de la catégorie
 * @apiSuccess (Succès : 200) {String} nom Nom de la catégorie
 * @apiSuccess (Succès : 200) {String} description Description de la catégorie
 * @apiSuccess (Succès : 200) {Link}   links-ingredients lien vers la liste d'ingrédients de la catégorie
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *     {
 *        categorie : {
 *            "id"  : 4 ,
 *            "nom" : "Fromages",
 *            "description" : "Nos fromages bios et au lait cru. En majorité des AOC."
 *        },
 *        links : {
 *            "ingredients" : { "href" : "/eloquent_sandwichs/apiPublic/categorie/4/ingredients }
 *        }
 *     }
 *
 * @apiError (Erreur : 404) CategorieNotFound Categorie inexistante
 *
 * @apiErrorExample {json} exemple de réponse en cas d'erreur
 *     HTTP/1.1 404 Not Found
 *
 *     {
 *       "error": "ressource not found : /VMProject/php.iut.local/php.iut.local/www/Project/api/categories/50"
 *     }
*/

/**
 * @apiGroup Categories
 * @apiName GetCategingredients
 * @apiVersion 0.1.0
 *
 * @api {get} /categories/id/ingredients  Accès au ressource ingrédients d'une catégorie
 *
 * @apiDescription Accès au ressource de type ingrédients
 * permet d'accéder à la représentation des ressources ingrédients de la categorie désignée.
 * Retourne une représentation json de la ressource, incluant son id, nom, id de la catégorie, 
 * sa description, fournisseur et image.
 *
 *
 * @apiParam {Number} id Identifiant unique de la catégorie
 *
 *
 * @apiSuccess (Succès : 200) {Number} id Identifiant de l'ingrédient
 * @apiSuccess (Succès : 200) {String} nom Nom de l'ingrédient
 * @apiSuccess (Succès : 200) {Number} cat_id Identifiant de la catégorie
 * @apiSuccess (Succès : 200) {String} description Description de l'ingrédient
 * @apiSuccess (Succès : 200) {String} fournisseur Nom du fournisseur de l'ingrédient
 * @apiSuccess (Succès : 200) {String} img Image de l'ingrédient
 *
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *		
 * [
 *   [
 *       {
 *            "id"  : 1 ,
 *            "nom" : "laitue",
 *            "cat_id" : 1,
 *            "description" : "belle laitue verte",
 *            "fournisseur" : "ferme \"la bonne salade\"",
 *            "img" : null
 *        },
 *        {
 *            ...
 *        }
 *     }
 *
 * @apiError (Erreur : 404) CategorieNotFound Categorie inexistante
 *
 * 
*/

/**
 * @apiGroup Ingredients
 * @apiName GetIngredients
 * @apiVersion 0.1.0
 *
 * @api {get} /ingredients/id  Accès a une ressource ingrédients
 *
 * @apiDescription Accès a une ressource de type ingrédients
 * permet d'accéder à la représentation d'une ressource ingrédient .
 * Retourne une représentation json de la ressource, incluant son nom et
 * id.
 *
 *
 * @apiSuccess (Succès : 200) {Number} id Identifiant de l'ingrédient
 * @apiSuccess (Succès : 200) {String} nom Nom de l'ingrédient
 * @apiSuccess (Succès : 200) {Number} id Identifiant de la catégorie
 * @apiSuccess (Succès : 200) {String} nom Nom de la catégorie auquel appartient l'ingrédient
 * @apiSuccess (Succès : 200) {String} description Description de la catégorie
 * @apiSuccess (Succès : 200) {String} description Description de l'ingrédient
 * @apiSuccess (Succès : 200) {String} fournisseur Nom du fournisseur de l'ingrédient
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *	  {
 *      "Ingredient" : {
 *        "id" : 2,
 *        "Nom" : "roquette",
 *        "Categorie" : {
 *           "id" : 1,
 *           "Nom" : "salades",
 *           "Description" : "Nos bonnes salades, fraichement livrées par nos producteurs bios et locaux"
 *        },
 *        "Description" : "La roquette qui pète ! Bon, bien sûr et sauvage",
 *        "Description" : "La roquette qui pète ! Bo, bien sûr et sauvage",
 *        "Fournisseur" : "ferme \"la bonne salade\""
 *      }
 *    }
 *
 * @apiError (Erreur : 405) Ressource not allowed
 *
 * @apiErrorExample {json} Exemple de réponse en cas d'erreur
 *     HTTP/1.1 405 Bad Ressource not allowed
 *
 *  {
 *    "status": {
 *      "405": "ressource not allowed : /eloquent_sandwichs/apiPublic/ingredient/21"
 *    }
 *  }
 *
*/

/**
* @apiGroup Commandes
* @apiName postCommandes
* @apiVersion 0.1.0
*
* @api {post} /commande[/]  Création
*
* @apiDescription On créait une commande
*
* Retourne une représentation json de la ressource ajoutée
* Un label, des coordonnées ainsi qu'un nom doivent être fournis
*
*
* @apiParam  (Paramètres requis) {String} nom Nom du sandwich que l'on veut
* @apiParam  (Paramètres requis) {Float} date_livraison date de livraison du sandwich
*
*
* @apiSuccess (Succès : 200) {String} name Nom du sandwich créer
* @apiSuccess (Succès : 200) {date} date_livraison Date de livraison du sandwich au destinataire
* @apiSuccess (Succès : 200) {timezone_type} timezone_type retour du type de la zone
* @apiSuccess (Succès : 200) {timezone} timezone retour de l'endroit de la commande
* @apiSuccess (Succès : 200) {float} Montant montant de la commande effectuée
* @apiSuccess (Succès : 200) {String} Token token attribué à la commande
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*   {
*    "Commande": {
*      "name" : "panini2",
*      "Date de livraison" : "2017-02-24 13:34:00",
*      "timezone_type" : "3",
*      "timezone" : "Europe/Paris",
*      "Montant" : "0",
*      "Token" : "i31c1q9konheqo4tl-e2foflm7ignsw*"  
*      }
*   }
*
* @apiError (Erreur : 400) BadRequest
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 400 Bad Request
*
*  {
*    "status": {
*      "400": "Command reation : missing data (all)"
*    }
*  }
*
*
* @apiError (Erreur : 400) BadRequest
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 400 Bad Request
*
*  {
*    "status": {
*      "400": "Command reation : missing data (nom)"
*    }
*  }
*
*
* @apiError (Erreur : 400) BadRequest
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 400 Bad Request
*
*  {
*    "status": {
*      "400": "Command reation : missing data (date_livraison)"
*    }
*  }
*
*
* @apiError (Erreur : 400) BadRequest
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 400 Bad Request
*
*  {
*    "status": {
*      "400": "Command reation : invalid argument (date)"
*    }
*  }
*
*
* @apiError (Erreur : 401) NotAllowed
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 Not allowed
*
*  {
*    "status": {
*      "401": "ressource not allowed."
*    }
*  }
*/

/**
* @apiGroup Commandes
* @apiName deleteCommande
* @apiVersion 0.1.0
*
* @api {delete} /commande/{token}/supprimer[/]  Supprimer
*
* @apiDescription On supprime une commande
*
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*  {
*    "status": {
*      "200": "La commande a été supprimé avec succès"
*    }
*  }
*
* @apiError (Erreur : 404) Notfound Commande not found
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 Not Found
*
*  {
*    "status": {
*      "404": "Commande not found."
*    }
*  }
*
*/

/**
* @apiGroup Commandes
* @apiName deleteSandwich
* @apiVersion 0.1.0
*
* @api {delete} /commande/{token}/supprimer/{id}[/]  SuppSand
*
* @apiDescription On supprime un sandwich dans une commande
*
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*  {
*    "status": {
*      "200": "Le sandwich a bien été supprimé de la commande"
*    }
*  }
*
* @apiError (Erreur : 404) Notfound Sandwich not found
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 Not Found
*
*  {
*    "status": {
*      "404": "Sandwich not found."
*    }
*  }
*
*/

/**
 * @apiGroup Commandes
 * @apiName GetCommande
 * @apiVersion 0.1.0
 *
 * @api {get} /commande/{token}/id  Obtenir l'état d'une commande
 *
 * @apiDescription Obtenir l'état d'une commande avec le détail de celle-ci
 * le nom du sandwich, le type de pain, la taille et le prix
 *
 *
 * @apiSuccess (Succès : 200) {String} nom Nom du sandwich
 * @apiSuccess (Succès : 200) {String} type_pain le type de pain qui est choisi
 * @apiSuccess (Succès : 200) {String} Taille la taille du pain qui est choisi pour la commande 
 * @apiSuccess (Succès : 200) {Float} Prix Prix du sandwich qui a été choisi
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *	  {
 *      "Commande" : {
 *        "Nom" : "fromage",
 *        "Type de pain" : "",
 *        "Taille" : "ogre",
 *        "Prix" : "3.40",
 *        }
 *    }
 *
 * @apiError (Erreur : 403) Forbidden NoToken
 *
 * @apiErrorExample {json} Exemple de réponse en cas d'erreur
 *     HTTP/1.1 403 Forbidden
 *
 *  {
 *    "status": {
 *      "403": "no token or invalid token"
 *    }
 *  }
 *
*/

/**
 * @apiGroup Commandes
 * @apiName GetFacture
 * @apiVersion 0.1.0
 *
 * @api {get} /commande/{token}/facture  Obtenir la facture d'une commande
 *
 * @apiDescription Obtenir la facture d'une commande avec le détail de celle-ci
 * la date de livraison, le montant, le sandwich en question et le nom de la commande
 *
 *
 * @apiSuccess (Succès : 200) {date} Date livraison Date de livraison pour le sandwich
 * @apiSuccess (Succès : 200) {Float} Montant Montant de la livraison au total 
 * @apiSuccess (Succès : 200) {Array} Sandwichs Tableau contenant les informations du sandwich commandé
 * @apiSuccess (Succès : 200) {String} Nom Type de fromage dedans
 * @apiSuccess (Succès : 200) {String} Type pain Le type de pain en question
 * @apiSuccess (Succès : 200) {String} Taille La taille du pain qui a été choisi   
 * @apiSuccess (Succès : 200) {Float} Prix Prix du sandwich qui a été choisi
 * @apiSuccess (Succès : 200) {String} Commande
 * @apiSuccess (Succès : 200) {String} Nom Nom de la commande pour les sandwichs
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *{
 *  "0": {
 *    "Date livraison :": "2017-02-20 20:40:00"
 *  },
 *  "1": {
 *    "Montant" : "20.5"
 *  },
 *  "2": {
 *    "Sandwichs :": [
 *      "Nom" : "fromage",
 *      "Type Pain" : "Pain", 
 *      "Taille" : "ogre",
 *      "Prix" : "3.80",
 *    ]
 * },
 *  "Commande": {
 *    "Nom :": "test1"
 *  }
 *}
 *
 * @apiError (Erreur : 403) Forbidden NoToken
 *
 * @apiErrorExample {json} Exemple de réponse en cas d'erreur
 *     HTTP/1.1 403 Forbidden
 *
 *  {
 *    "status": {
 *      "403": "no token or invalid token"
 *    }
 *  }
 *
*/

/**
* @apiGroup Commandes
* @apiName updateCommande
* @apiVersion 0.1.0
*
* @api {put} /commande/{token}/livraison[/]  Modifier
*
* @apiDescription On modifie une livraison
*
* Retourne une représentation json de la ressource ajoutée
* Un format date doit être fournis
*
*
* @apiParam  (Paramètres possible) {date} date_livraison date de la livraison modifié
*
*
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*   {
*    "Votre Commande a bien été mis à jour"
*   }
*
* @apiError (Erreur : 404) NotFound Commande not found
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 Not Found
*
*  {
*    "status": {
*      "404": "Commande not found."
*    }
*  }
*
*/

/**
* @apiGroup Commandes
* @apiName payerCommande
* @apiVersion 0.1.0
*
* @api {put} /commande/{token}/paiement[/]  Payer
*
* @apiDescription On paye une commande avec coordonnées bancaires
*
* Retourne une représentation json de la ressource ajoutée
* Un champ nom_prenom, numero_carte, date_expiration, code_verification et 
* doivent être fournis pour payer la commande
*
*
* @apiParam (Paramètres possible) {String} nom_prenom nom et prenom de la personne qui paye
* @apiParam (Paramètres possible) {Float} numero_carte numero de la carte pour payer
* @apiParam (Paramètres possible) {date} date_expiration date d'expiration de la carte
* @apiParam (Paramètres possible) {Float} code_verification code de verification pour payer
* @apiParam (Paramètres possible) {Float} montant montant de la commande a payer
*
*
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*   {
*    "La commande a bien été payée"
*   }
*
* @apiError (Erreur : 404) NotFound Commande not found
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 NotFound
*
*  {
*    "status": {
*      "404": "Commande not found."
*    }
*  }
*
*
* @apiError (Erreur : 400) BadRequest Commande reation
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 400 BadRequest
*
*  {
*    "status": {
*      "400": "Commande reation : missing data (nom_prenom, numero_carte, date_expiration, code_verification, montant)."
*    }
*  }
*
*
* @apiError (Erreur : 401) NotAllowed Ressource not allowed
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 401 NotAllowed
*
*  {
*    "status": {
*      "401": "Ressource not allowed."
*    }
*  }
*
*/

/**
* @apiGroup Commandes
* @apiName ajouterCommande
* @apiVersion 0.1.0
*
* @api {put} /commande/{token}/sandwich[/]  Ajouter
*
* @apiDescription On ajoute un sandwich à une commande
*
* Retourne une représentation json de la ressource ajoutée
* Un champ nom, type_pain, taille, ingredients doivent être fournis afin de préparer le sandwich
*
*
* @apiParam (Paramètres possible) {String} nom nom du sandwich a ajouté
* @apiParam (Paramètres possible) {String} type_pain le type de pain qui est choisi
* @apiParam (Paramètres possible) {String} taille la taille du pain pour le client
* @apiParam (Paramètres possible) {String} ingredients les ingredients dans le sandwich
*
*
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*   {
*    "Ajout du sandwich" : [
*        "Nom Sandwich" : "Bavarois"
*        "Type de Pain" : "Pain",
*        "Taille" : "Ogre",
*        "prix" : "25.6",
*       ]    	 
*    {
*        "Votre Sandwich a été ajouté dans la commande"
*    } 
*   }
*
* @apiError (Erreur : 404) NotFound Commande not found
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 Not Found
*
*  {
*    "status": {
*      "404": "Commande not found."
*    }
*  }
*
* @apiError (Erreur : 400) BadRequest 
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 400 BadRequest
*
*  {
*    "status": {
*      "400": "Cette taille n'existe pas."
*    }
*  }
*/

/**
* @apiGroup Commandes
* @apiName modifierSandwich
* @apiVersion 0.1.0
*
* @api {put} /commande/{token}/livraison[/]  ModifierSandwich
*
* @apiDescription On modifie un sandwich
*
* Retourne une représentation json de la ressource ajoutée
* Un format date doit être fournis
*
*
* @apiParam  (Paramètres possible) {Float} id Id du sandwich à modifier
*
*
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*   {
*    "Etat du sandwich" : 
*       {
*	      "Nom" : "Barisien",
*         "Montant" : "3.60",
*         "Type de pain" : "Sandwich"
*       }
*   }
*
* @apiError (Erreur : 404) NotFound Sandwich not found
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 Not Found
*
*  {
*    "status": {
*      "404": "Commande not found."
*    }
*  }
*
*
* @apiError (Erreur : 403) Forbidden NoToken
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 403 Forbidden
*
*  {
*    "status": {
*      "403": "no token or invalid token"
*    }
*  }
*
*/

/**
* @apiGroup API-PRIVEE
* @apiName GetDetailCommande
* @apiVersion 0.1.0
*
* @api {get} /commande/{id}[/]  Accès au détail complet de la commande
*
* @apiDescription Accès a une ressource de type commande
* trié par date de livraison et ordre de création
*
* @apiSuccess (Succès : 200) {date} Date livraison date de livraison de la commande
* @apiSuccess (Succès : 200) {Float} Montant Montant total de la commande
* @apiSuccess (Succès : 200) {Array} Sandwichs Tableau de sandwichs avec les détails de chacuns commandés.
* @apiSuccess (Succès : 200) {String} Nom Le nom des sandwichs qui sont commandés
* @apiSuccess (Succès : 200) {String} Commande Le nom de la commande
*
*
* @apiSuccessExample {json} exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*	  {
*	     "0" : {
*	        "Date livraison" : "2017-02-02 19:40:00"
*         },
*        "1" : {
*	        "Montant" : "52.700000762939"
*         },
*        "2" : {
*	        "Sandwichs" : "[
*             "Nom" : "Fromage",
*             "Type" : "",
*             "Prix" : "4.50",
*             
*             "Nom" : "Raclette",
*             "Type" : "",
*             "Prix" : "7.80",
*             
*             "Nom" : "Raclette",
*             "Type" : "",
*             "Prix" : "7.80",
*             
*             "Nom" : "Raclette",
*             "Type" : "",
*             "Prix" : "7.80",
*
*             "Nom" : "Raclette",
*             "Type" : "",
*             "Prix" : "3.80",
*             
*             "Nom" : "Raclette",
*             "Type" : "",
*             "Prix" : "3.80",
*           ]"
*         },
*         "Commande" : {
*	        "Nom" : "Test",
*         }
*     }
*
*/

/**
 * @apiGroup API-PRIVEE
 * @apiName GetListeDate
 * @apiVersion 0.1.0
 *
 * @api {get} /commandes/liste/date[/]  Obtenir la liste des commandes
 *
 * @apiDescription Obtenir la liste complète des commandes, trièes par date de livraison et date de création
 *
 *
 * @apiSuccess (Succès : 200) {Number} id Id de la commande
 * @apiSuccess (Succès : 200) {String} nom Nom de la commande
 * @apiSuccess (Succès : 200) {date} date_livraison Date de livraison de la commande
 * @apiSuccess (Succès : 200) {Int} etat_avancement Etat de la commande à un instant précis
 * @apiSuccess (Succès : 200) {Int} etat_paiement Etat du paiement de la commande
 * @apiSuccess (Succès : 200) {Float} montant Prix total de la commande
 * @apiSuccess (Succès : 200) {String} Token token de la commande
 * @apiSuccess (Succès : 200) {date} updated_at Date de Mise à jour de la commmande
 * @apiSuccess (Succès : 200) {date} created_at Date de Création de la commande
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *[
 *   {
 *	   "id": 8,
 *     "nom": "test",
 *     "date_livraison": "2017-02-02 19:40:00",
 *     "etat_avancement": 0,
 *     "etat_paiement": 0,
 *     "montant": 52.700000762939,
 *     "token": "test",
 *     "updated_at": "2017-02-21 15:51:51",
 *     "created_at": "2017-02-21 14:37:49"
 *   }
 *]
 *
 * @apiError (Erreur : 404) Ressource NotFound
 *
 * @apiErrorExample {json} Exemple de réponse en cas d'erreur
 *     HTTP/1.1 404 NotFound
 *
 *  {
 *    "status": {
 *      "404": "Ressource not Found"
 *    }
 *  }
 *
*/

/**
 * @apiGroup API-PRIVEE
 * @apiName GetLimit
 * @apiVersion 0.1.0
 *
 * @api {get} /commandes/{offset}/{limit}[/]  Obtenir la liste des commandes avec pagination
 *
 * @apiDescription Obtenir la liste complète des commandes avec pagination, offset et limit passées dans l'URL
 *
 *
 * @apiSuccess (Succès : 200) {Array} Tableau avec le contenu
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *     []
 *
 * @apiError (Erreur : 400) BadRequest
 *
 * @apiErrorExample {json} Exemple de réponse en cas d'erreur
 *     HTTP/1.1 400 BadRequest
 *
 *  {
 *    "status": {
 *      "400": "command reation : arguments limit cannot be 0"
 *    }
 *  }
 *
*/

/**
 * @apiGroup API-PRIVEE
 * @apiName GetFiltrage
 * @apiVersion 0.1.0
 *
 * @api {post} /commandes/filtrage[/]  Liste des commandes avec filtrage
 *
 * @apiDescription Liste des commandes , avec filtrage sur l'état et sur la date de livraison
 *
 *
 * @apiSuccess (Succès : 200) {Array} Tableau avec le contenu
 *
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *
 *     {
 *	      "Commande" : {
 *	          "etat" : "0",
 *            "date" : "2017-02-20 14:45",
 *        }
 *     }
 *
 * @apiError (Erreur : 400) BadRequest
 *
 * @apiErrorExample {json} Exemple de réponse en cas d'erreur
 *     HTTP/1.1 400 BadRequest
 *
 *  {
 *    "status": {
 *      "400": "command reation : missing data (etat)"
 *    }
 *  }
 *
*/

/**
* @apiGroup API-PRIVEE
* @apiName PutEtat
* @apiVersion 0.1.0
*
* @api {put} /commande/{id}/etat[/]  ModifierEtatCommande
*
* @apiDescription On modifie l'état d'une commande
*
* Retourne une représentation json de la ressource ajoutée
* Un format état doit être fourni
*
*
* @apiParam  (Paramètres possible) {Int} etat Etat de la commande à modifier
*
*
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*   {
*    "La commande n°8 a été mise à jour",
*   }
*
* @apiSuccessExample {json} Exemple de réponse en cas de succès
*     HTTP/1.1 200 OK
*
*   {
*    "La commande n°8 ne peut être mise à jour car déjà en cours de livraison",
*   }
*
* @apiError (Erreur : 404) NotFound Commande not found
*
* @apiErrorExample {json} Exemple de réponse en cas d'erreur
*     HTTP/1.1 404 Not Found
*
*  {
*    "status": {
*      "404": "Commande not found."
*    }
*  }
*
*/