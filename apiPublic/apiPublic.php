<?php
/*azertyuio*/
require '../vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \sandwich\models\AppInit as AppInit;

use \sandwich\models\Ingredient;
use \sandwich\models\Categorie;
use \sandwich\models\Commande;
use \sandwich\models\Sandwich;

use \sandwich\controllers\Test;
use \sandwich\controllers\CategorieController;
use \sandwich\controllers\IngredientController;
use \sandwich\controllers\CommandeController;


AppInit::bootEloquent('../conf/conf.ini');

$conf=['settings'=>['displayErrorDetails'=>true]];

$app = new \Slim\App(new \Slim\Container($conf));

function checkToken ( Request $rq, Response $rs, callable $next ) {
	//récupérer le token de la partie dans la route 
	//$id = $rq->getAttribute('route')->getArgument( 'id');

	$token = $rq->getAttribute('route')->getArgument('token');

	// vérifier que le token correspond à la commande
	try {
		\sandwich\models\Commande::where('token', '=',$token)->firstOrFail();
		return $next($rq, $rs);
	}
	catch (\Exception $e)
	{
		$status = 403;

		$content = json_encode(['error'=>'no token or invalid token']);

		$ab = new \sandwich\controllers\AbstractController;
		return $ab->json_error($rs, $status, $content);
	};
	
}

$app->get('/hello/{name}', 
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\Test($this))
			->getHello($req, $resp, $args);
	}
);

//Accés aux catégories
$app->get('/categories[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CategorieController($this))
			->getCategories($req, $resp, $args);
	}
)->setName('categories');


//Lister les catégories d'un ingrédient
$app->get('/categorie/{id}/ingredients[/]',
		function(Request $req, Response $resp, $args){
			return (new \sandwich\controllers\CategorieController($this))
				->getIngredCategorie($req, $resp, $args);
		}
	)->setName('categ2ingre');

//Détail de la catégorie
$app->get('/categorie/{id}[/]',
		function(Request $req, Response $resp, $args){
			return (new \sandwich\controllers\CategorieController($this))
				->getCategorie($req, $resp, $args);
		}
	)->setName('categorie');

//Liste d'un ingrédient en particulier
$app->get('/ingredient/{id}[/]',
	function(Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\IngredientController($this))
				->getIngredient($req, $resp, $args);
	})->setName('ingred');

//Création d'une commande
$app->post('/commande[/]',
	function(Request $req, Response $resp, $args) {
		return(new \sandwich\controllers\CommandeController($this))
				->postCommande($req, $resp, $args);
	})->setName('cmd');

//Suppression d'un sandwich dans une commande
$app->delete('/commandes/{token}/sandwich/{id}[/]',
	function(Request $req, Response $resp, $args){
		return(new \sandwich\controllers\CommandeController($this))
				->deleteSandwich($req, $resp, $args);
	})->setName('deleteSand')->add('checkToken');

//Suppression d'une commande
$app->delete('/commande/{token}/supprimer[/]',
	function(Request $req, Response $resp, $args){
		return(new \sandwich\controllers\CommandeController($this))
				->deleteCommande($req, $resp, $args);
	})->setName('deleteComm')->add('checkToken');

//Ajouter un sandwich a une commande
$app->put('/commande/{token}/sandwich[/]',
	function(Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
				->addSandwichCommande($req, $resp, $args);
	})->setName('addSand')->add('checkToken');

//obtenir une description complète du contenu d'une commande
$app->get('/commande/{token}/detail[/]',
	function(Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
				->detailCommande($req, $resp, $args);
	})->setName('detail')->add('checkToken');

//Obtenir la facture d'une commande
$app->get('/commande/{token}/facture[/]',
	function(Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
				->factureCommande($req, $resp, $args);
	})->setName('facture')->add('checkToken');

//Obtenir l'état d'une commande
$app->get('/commande/{token}[/]',
	function(Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
				->etatCommande($req, $resp, $args);
	})->setName('det')->add('checkToken');

//Modifier la date de livraison d'une commande
$app->put('/commande/{token}/livraison[/]',
	function(Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
				->putCommande($req, $resp, $args);
	})->setName('putcommande')->add('checkToken');

//Payer une commande
$app->put('/commande/{token}/paiement[/]',
	function(Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
				->payerCommande($req, $resp, $args);
	})->setName('payercommande')->add('checkToken');

//Modification du sandwich
$app->put('/commande/{token}/modifier[/]',
	function(Request $req, Response $resp, $args){
		return (new \sandwich\controllers\CommandeController($this))
				->modifSandwich($req, $resp, $args);
	})->setName('modifSand');


$app->run();

