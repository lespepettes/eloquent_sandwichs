angular.module('sandwich').controller('IngredientsController',
	['$scope', '$http', '$rootScope',

	function($scope,$http,$rootScope){
		
		$scope.remove=function(ingredId){
			$rootScope.ingredients.forEach(function(t){
				if(t.id==ingredId){
					$rootScope.ingredients.splice($rootScope.ingredients.indexOf(t), 1);
				}
			});
		},

		$scope.supprimer=function(id){
			$id=id;
			//console.log($rootScope.route+'apiPrivate/ingredient', {id:$id});
			$http.delete($rootScope.route+'apiPrivate/ingredient/'+$id) 
			.then(function(response){
					$scope.remove($id);
					console.log(response);
				},function(error){
					console.log(error);
				}
			);			
		},

		$scope.ajouter=function(){
			if($scope.descr==undefined){
				$scope.descr='NULL';
			}
			if($scope.fournisseur==undefined){
				$scope.fournisseur='NULL';
			}
			if($scope.img==undefined){
				$scope.img='NULL';
			}
			
			$http.post($rootScope.route+'apiPrivate/ingredient', {nom: $scope.nom, cat: $scope.cat, description: $scope.descr, fournisseur: $scope.fournisseur, image: $scope.img})
			.then(function(response){
					alert(response.data);
				},function(error){
					console.log(error);
				}
			);
		}
	}
]);