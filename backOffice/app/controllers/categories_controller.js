angular.module('sandwich').controller('CategoriesController',
		['$scope', '$http', '$rootScope',

		function($scope, $http, $rootScope){

			$http.get($rootScope.route+'apiPublic/categories')
			.then(function(response){
				$scope.categories = [];
	
				response.data.categories.forEach(function(data){
					$scope.categories.push(data);
				});
			},function(error){
				console.log('error');
			});

			$scope.afficher=function(id){
				$http.get($rootScope.route+'apiPublic/categorie/'+id+'/ingredients')
				.then(function(response){
					//$rootScope.categorieSelect=id;
					//$rootScope.selectedcategorie=id;
					$rootScope.ingredients =[];
					
					response.data.ingredient.forEach(function(data){
						$rootScope.ingredients.push(data);
					});
				});
			}

		}
	]);
