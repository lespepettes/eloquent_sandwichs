# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version 0.1.0
* [https://bitbucket.org/lespepettes/eloquent-sandwichs/downloads](Link URL)

### How do I get set up? ###

* *Database configuration :*
  Il est nécessaire d'installer la base de donnée grâce au fichier TD1WebServer.sql et de créer un dossier conf au même niveau que les dossier API et y insérer un fichier conf.ini avec comme code php :   
```
#!php
driver = 'mysql';
host = 'localhost';
database = 'TD1WebServer';
username = 'root';
password = 'root';
charset = 'utf8';
collation = 'utf8_unicode_ci';
prefix = '';

```
* *Pour l'API publique:*
    * Avec localhost : http://localhost/eloquent-sandwichs/apiPublic/{route}
    * Avec vagrant : http://atelier.local/eloquent-sandwich/apiPublic/{route}

* *Pour l'API publique:*
    * Avec localhost : http://localhost/eloquent-sandwichs/apiPrivate/{route}
    * Avec vagrant : http://atelier.local/eloquent-sandwich/apiPrivate/{route}


* *Pour l'espace backoffice :*
     * Avec localhost : http://localhost/eloqent-sandwichs/backOffice/
     * Avec vagrant : http://atelier.local/eloquent-sandwichs/backOffice/
     * Identifiant : test
     * Mot de passe : test

*  *Pour l'API :*
    * Avec localhost : http://localhost/eloquent-sandwichs/docs-api/index.html
    * Avec vagrant : http://atelier.local/eloquent-sandwichs/docs-api/index.html

### Who do I talk to? ###

* Varnerot Benjamin - Dagnet Florian - Barre Kévin