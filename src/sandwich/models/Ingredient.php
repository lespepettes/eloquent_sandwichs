<?php

namespace sandwich\models;

class Ingredient extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'ingredient';
	protected $primarykey = 'id';

	public $timestamps=false;
	
	public function CategIngred(){
		return $this->belongsTo('\sandwich\models\Categorie', 'cat_id');
	}

	public function ingredSandwich(){
		return $this->belongsToMany('\sandwich\models\Sandwich','sandwich2ingredient', 'id_ingredient', 'id_sandwich');
	}
}