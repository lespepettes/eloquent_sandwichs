<?php

namespace sandwich\models;

class Sandwich2Ingredient extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'sandwich2ingredient';
	protected $primarykey = 'id';
	
	public $timestamps=false;
	
	public function idSandwich(){
		return $this->belongTo('\sandwich\models\Sandwich', 'id');
	}

	public function idIngredient(){
		return $this->belongTo('\sandwich\models\Ingredient', 'id');
	}
}