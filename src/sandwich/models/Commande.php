<?php

namespace sandwich\models;

class Commande extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'commande';
	protected $primarykey = 'id';

	public function commandeSandwich(){
		return $this->belongsToMany('\sandwich\models\Sandwich', 'sandwich2commande', 'id_commande', 'id_sandwich');
	}

	
}