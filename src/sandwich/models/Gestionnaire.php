<?php

namespace sandwich\models;

class Gestionnaire extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'gestionnaire';
	protected $primarykey = 'id';

}