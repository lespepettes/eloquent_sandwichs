<?php

namespace sandwich\models;

class Sandwich2Commande extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'sandwich2commande';
	protected $primarykey = 'id';
	
	public $timestamps=false;
	
	public function idSandwich(){
		return $this->belongTo('\sandwich\models\Sandwich', 'id');
	}

	public function idCommande(){
		return $this->belongTo('\sandwich\models\Commande', 'id');
	}
}