<?php

namespace sandwich\models;

class Sandwich extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'sandwich';
	protected $primarykey = 'id';

	public $timestamps=false;

	public function sandwichCommande(){
		return $this->belongsToMany('\sandwich\models\Commande', 'sandwich2commande', 'id_sandwich', 'id_commande');
	}

	public function sandwichIngred(){
		return $this->belongsToMany('\sandwich\models\Ingredient','sandwich2ingredient', 'id_sandwich', 'id_ingredient');
	}
}