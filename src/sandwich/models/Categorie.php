<?php

namespace sandwich\models;

class Categorie extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'categorie';
	protected $primarykey = 'id';

	public function Ingred(){
		return $this->hasMany('\sandwich\models\Ingredient', 'cat_id');
	}
	
}