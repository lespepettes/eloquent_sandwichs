<?php

namespace sandwich\controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \sandwich\models\AppInit as AppInit;
use \sandwich\models\Gestionnaire;

class GestionnaireController extends AbstractController{ 

	public function getConnexion(Request $req, Response $resp, $args){
		try{

			$co = Gestionnaire::where('identifiant', '=', $args['identifiant'])->where('password', '=', $args['password'])->firstorFail();

			$status = 200;

			$content = json_encode("Connexion réussie");

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('Connexion')]);

			$this->json_error($resp, $status, $content);
		}
	}
}