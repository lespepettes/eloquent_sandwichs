<?php

namespace sandwich\controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \sandwich\models\AppInit as AppInit;

use \sandwich\models\Ingredient as Ingredient;
use \sandwich\models\Categorie as Categorie;
use \sandwich\models\Commande;
use \sandwich\models\Sandwich as Sandwich;
use \sandwich\models\Sandwich2Commande as Sandwich2Commande;
use \sandwich\models\Sandwich2Ingredient as Sandwich2Ingredient;

class CommandeController extends AbstractController {

	public function postCommande(Request $req, Response $resp, $args){
		
		try{		
			$data = $req->getParsedBody();


			if(empty($data)) {
				return $this->json_error($resp, 400, 'command reation : missing data (all)');
			}

			if (!isset($data['nom'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (nom)');
			}
			if (!isset($data['date_livraison'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (date_livraison)');
			}

			$c = new Commande;

			$c->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
			$date = \DateTime::createFromFormat('Y-m-d H:i', $data['date_livraison']);

			if (!$date) {
				return $this->json_error($resp, 400, 'command reaction : invalid argument (date)');
			}

			$c->date_livraison = $date;
			$c->etat_avancement = 0;
			$c->etat_paiement = 0; 
			$c->montant = 0;


			$tokenFactory = new \RandomLib\Factory;
			$generator = $tokenFactory->getMediumStrengthGenerator();
			$randomString = $generator->generateString(32, 'abcdefghijklmnopqrstuvwyz123456789-*_');

			$c->token = $randomString;

			$c->save();

			$status = 200;

			$content = json_encode(json_encode(["Commande"=>["Name"=>$c["nom"],"Date de livraison"=>$c["date_livraison"],"Montant"=>$c["montant"],"Token (NOTEZ LE BIEN)"=>$c["token"]]]));
			
			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e){
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('cmd', ['id'=>$args['id']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function addSandwichCommande(Request $req, Response $resp, $args){

		try{
			$data = $req->getParsedBody();

			$prix = null;

			$commande = Commande::where('token', '=', $args["token"])->firstOrFail();


			if($commande['etat_avancement']==1){

				$status = 200;

				$content = json_encode(["Trop tard"]);

				$this->json_success($resp, $status, $content);
			}

			if($data['taille']=="petite faim"){
				$prix = 3.80;
			}
			if($data['taille']=="moyenne faim"){
				$prix = 4.50;
			}
			if($data['taille']=="grosse faim"){
				$prix = 5.90;
			}
			if($data['taille']=="ogre"){
				$prix = 7.80;
			}

			if($data['taille'] != "petite faim"||"moyenne faim"||"grosse faim"||"ogre"){

				$status = 400;

				$content = json_encode(["Cette taille n'existe pas"]);

				$this->json_error($resp, $status, $content);
			}

			/* AJOUT DU SANDWICH */
			$sand = new Sandwich;
			$sand->nom=$data['nom'];
			$sand->type_pain=$data['type_pain'];
			$sand->taille=$data['taille'];
			$sand->prix=$prix;
			$sand->save();

			/* AJOUT LIEN SAND 2 COM */
			$s2c = new Sandwich2Commande;
			$s2c->id_sandwich=$sand['id'];
			$s2c->id_commande=$commande['id'];
			$s2c->save();

			/* MISE A JOUR DU MONTANT */
			$prixfinal = $prix+$commande['montant'];
			$commande = Commande::where('token', '=', $args["token"])->update(['montant'=>$prixfinal]);
				
			/* AJOUT LIEN SAND 2 INGRED */
			foreach ($data['ingredients'] as $ingred) {
				$s2i = new Sandwich2Ingredient;
				$s2i->id_sandwich=$sand['id'];
				$s2i->id_ingredient=$ingred;
				$s2i->save();
			}

			$status = 200;

			$content = json_encode(["Ajout du sandwich "=>[
													"Nom sandwich"=>$data["nom"],
													"Type de pain"=>$data["type_pain"],
													"taille"=>$data["taille"],
													"prix"=>$prix,
													"Dans la commande du token"=>$args["token"]
													]]);

			$this->json_success($resp, $status, $content);
			
		}
		catch(\Exception $e)
		{
			$status = 404;
			
			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('addSand', ['token'=>$args['token']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function modifSandwich(Request $req, Response $resp, $args){
		try{
			$data = $req->getParsedBody();
			$commande = Sandwich::where('id', '=', $data["id"])->update(['nom'=>"Barisien"]);
			$commande = Sandwich::where('id', '=', $data["id"])->update(['prix'=>"3.60"]);
			$commande = Sandwich::where('id', '=', $data["id"])->update(['type'=>"Sandwich"]);
			//$commande = Sandwich::where('id', '=', $data["id"])->update(['taille'=>"Ogre"]);
			//$commande = Sandwich::where('id', '=', $data["id"])->update(['description'=>"Un bon sandwich"]);

			$status = 200;
			$content = json_encode(["Etat du sandwich"=>[
													"Nom"=>$commande['nom'],
													"Montant"=>$commande['prix'],
													"Type de Pain"=>$commande['type']]]);

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{

			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('etat', ['token'=>$args['token']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function etatCommande(Request $req, Response $resp, $args){
	
		try{
			$etat = Commande::where('token', '=', $args["token"])->firstOrFail();

			$resp = $resp->withStatus(200)->withHeader('Content-Type', 'application/json');

			$resp->getBody()->write();

			$status = 200;

			$content = json_encode(["Etat de la commande"=>[
													"Date"=>$etat['date_livraison'],
													"Montant"=>$etat['montant'],
													"Etat avancement"=>$etat['etat_avancement'],
													"Etat paiement"=>$etat['etat_paiement']]]);

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('etat', ['token'=>$args['token']])]);

			$this->json_error($resp, $status, $content);
		}
	}


	public function putCommande(Request $req, Response $resp, $args) {

		try{
			$data = $req->getParsedBody();

			if(empty($data)) {
				return $this->json_error($resp, 400, 'Bad Request : missing data (Livraison)');
			}

			$livraison = $data["date_livraison"];

			$commande = Commande::where('token', '=', $args["token"])->update(['date_livraison'=>$livraison]);

			$status = 200;

			$content = json_encode('Votre commande a été mise a jour');

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('putcommande', ['token'=>$args['token']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function deleteSandwich(Request $req, Response $resp, $args){
		try{
			$commande = Commande::where('token', '=', $args["token"])->firstOrFail();
			$sandwich = Sandwich2Commande::where('id', '=', $args["id"])->firstOrFail();
			$sandwich->delete();
			return $this->json_success($resp, 200, "Le sandwich a bien été supprimé de la commande");
		}
		catch(\Exception $e){
			return $this->json_error($resp, 404, "Sandwich not Found");
		}
	}

	public function deleteCommande(Request $req, Response $resp, $args){
		try{
			$commande = Commande::where('token', '=', $args["token"])->firstOrFail();
			
			if($commande['etat_paiement']==1){
				return $this->json_error($resp, 400, "La commande a déjà été payé. Impossible de la supprimer");
			}
			
			Commande::where('token', '=', $args["token"])->delete();
			return $this->json_success($resp, 200, "La commande a été supprimé avec succès");	
		}
		catch(\Exception $e){
			return $this->json_error($resp, 404, "Commande not Found");
		}
	}
	

	public function payerCommande(Request $req, Response $resp, $args){

		try{
			$commande = Commande::where('token', '=', $args["token"])->firstOrFail();
			$data = $req->getParsedBody();

			if (!isset($data['nom_prenom'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (nom_prenom)');
			}
			if (!isset($data['numero_carte'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (numero_carte)');
			}
			if (!isset($data['date_expiration'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (date_expiration)');
			}
			if (!isset($data['code_verification'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (code_verification)');
			}
			if (!isset($data['montant'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (montant)');
			}
			if($data['montant']!=$commande['montant']){
				return $this->json_error($resp, 400, 'error montant');
			}
			if($commande['etat_paiement']==1){
				return $this->json_error($resp, 400, 'Le paiement a déjà été fait');
			}


			$commande = Commande::where('token', '=', $args["token"])->update(['etat_paiement'=>1]);

			$status = 200;

			$content = json_encode('La commande a bien été payée');

			$this->json_success($resp, $status, $content);

		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('payercommande', ['token'=>$args['token']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function detailCommande(Request $req, Response $resp, $args){
		try{
			$commande = Commande::where('token', '=', $args["token"])->firstOrFail();

			$sand = Sandwich2Commande::where('id_commande', '=', $commande['id'])->get();

			$detail = '';

			foreach ($sand as $s) {
				$sandwich = Sandwich::where('id', '=', $s['id_sandwich'])->firstOrFail();
				$detail = $detail."[Nom :".$sandwich['nom']."	Type Pain:".$sandwich['type_pain']."		Taille:".$sandwich['taille']."		Prix :".$sandwich['prix']."]";
			}
			$status = 200;

			$content = json_encode($detail);

			$this->json_success($resp, $status, $content);

		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('detail', ['token'=>$args['token']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function factureCommande(Request $req, Response $resp, $args){
		try{
			$commande = Commande::where('token', '=', $args["token"])->firstOrFail();

			$sand = Sandwich2Commande::where('id_commande', '=', $commande['id'])->get();

			$detail = '';

			foreach ($sand as $s) {
				$sandwich = Sandwich::where('id', '=', $s['id_sandwich'])->firstOrFail();
				$detail = $detail."Nom :".$sandwich['nom']."
						Type Pain:".$sandwich['type_pain']."
						Taille".$sandwich['taille']."	
						Prix :".$sandwich['prix'];
			}
			$status = 200;

			$content = json_encode(["Commande"=>["Nom :"=>$commande['nom']],["Date livraison :"=>$commande['date_livraison']],["Montant :"=>$commande['montant']],["Sandwichs :"=>[$detail]]]);

			$this->json_success($resp, $status, $content);

		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('facture', ['token'=>$args['token']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function getDetailCommande(Request $req, Response $resp, $args){
		try{
			$commande = Commande::where('id', '=', $args["id"])->firstOrFail();

			$sand = Sandwich2Commande::where('id_commande', '=', $commande['id'])->get();

			$detail = '';

			foreach ($sand as $s) {
				$sandwich = Sandwich::where('id', '=', $s['id_sandwich'])->firstOrFail();
				$detail = $detail."Nom :".$sandwich['nom']."
						Type :".$sandwich['type']."	
						Prix :".$sandwich['prix'];
			}

			$status = 200;

			$content = json_encode(["Commande"=>["Nom :"=>$commande['nom']],["Date livraison :"=>$commande['date_livraison']],["Montant :"=>$commande['montant']],["Sandwichs :"=>[$detail]]]);

			$this->json_success($resp, $status, $content);

		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('DetailCommande', ['id'=>$args['id']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function updateEtatCommande(Request $req, Response $resp, $args){
		try{
			$commande = Commande::where('id', '=', $args["id"])->firstOrFail();

			if($commande['etat_avancement']==0)
			{
				$commande = Commande::where('id', '=', $args["id"])->update(['etat_avancement'=>1]);

				$status = 200;

				$content = json_encode("La commande numéro ".$args['id']." a été mise à jour.");

				$this->json_success($resp, $status, $content);
			}

			if($commande['etat_avancement']==1)
			{
				$commande = Commande::where('id', '=', $args["id"])->update(['etat_avancement'=>2]);

				$status = 200;

				$content = json_encode("La commande numéro ".$args['id']." a été mise à jour.");

				$this->json_success($resp, $status, $content);
			}

			if($commande['etat_avancement']==2)
			{
				$commande = Commande::where('id', '=', $args["id"])->update(['etat_avancement'=>3]);

				$status = 200;

				$content = json_encode("La commande numéro ".$args['id']." a été mise à jour.");

				$this->json_success($resp, $status, $content);
			}

			if($commande['etat_avancement']==3)
			{
				$status = 200;

				$content = json_encode("La commande numéro ".$args['id']." ne peut être mise à jour car déjà en cours de livraison.");

				$this->json_success($resp, $status, $content);
			}
		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('EtatCommande', ['id'=>$args['id']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function getDateCommandes(Request $req, Response $resp, $args){
		try{
			$commande = Commande::orderBy('date_livraison', 'asc')->orderBy('created_at', 'asc')->get();

			
			$status = 200;

			$content = json_encode($commande->toArray());

			$this->json_success($resp, $status, $content);
			
		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('ListeDateCommande')]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function getListeCommandes(Request $req, Response $resp, $args){
		try{
			if($args['limit']==0)
			{
				return $this->json_error($resp, 400, 'command reation : arguments limit cannot be 0');
			}
			
			$commande = Commande::offset($args['offset'])->limit($args['limit'])->get();
			
			$status = 200;

			$content = json_encode($commande->toArray());

			$this->json_success($resp, $status, $content);
			
		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('ListeCommandes')]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function getFiltreCommandes(Request $req, Response $resp, $args){
		try{

			$data = $req->getParsedBody();

			if (!isset($data['etat'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (etat)');
			}

			if (!isset($data['date'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (date)');
			}
			
			$commande = Commande::where('etat_avancement', '=', $data['etat'])->where('date_livraison', '=', $data['date'])->get();
			
			$status = 200;

			$content = json_encode($commande->toArray());

			$this->json_success($resp, $status, $content);
			
		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('FiltreCommandes')]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function getCA(Request $req, Response $resp, $args){
		try{
			$date = date("Y-m-d");
			$ca = Commande::where('date_livraison', '=', $date.'%')->sum('montant');
			$nbr = Commande::where('date_livraison', '=', $date.'%')->count();
			
			$status = 200;

			$content = json_encode("Nombre de commande aujourd'hui : ".$nbr."  |  CA du jour : ".$ca);

			$this->json_success($resp, $status, $content);
			
		}
		catch(Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('CA')]);

			$this->json_error($resp, $status, $content);
		}
	}
}