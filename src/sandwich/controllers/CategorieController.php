<?php

namespace sandwich\controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \sandwich\models\AppInit as AppInit;

use \sandwich\models\Ingredient as Ingredient;
use \sandwich\models\Categorie as Categorie;
use \sandwich\models\Commande as Commande;
use \sandwich\models\Sandwich as Sandwich;


class CategorieController extends AbstractController{

	public function getCategories(Request $req, Response $resp, $args){
		try {
			$lc = Categorie::Select('id', 'nom')->get();

			$status = 200;

			$content = json_encode(["nb"=>$lc->count(),"categories"=>$lc->toArray()]);

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not found : ".$this->c['router']->pathfor('categories')]);

			$this->json_error($resp, $status, $content);
		}	
	}

	public function getIngredCategorie(Request $req, Response $resp, $args){
		try{
			$ingred = Ingredient::where('cat_id', '=', $args)->get();
			
			$status = 200;

			$content = json_encode(["nb"=>$ingred->count(),"ingredient"=>$ingred->toArray()]);

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not found : ".$this->c['router']->pathfor('categ2ingre')]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function getCategorie(Request $req, Response $resp, $args){
		try{
			$categ = Categorie::where('id', '=', $args['id'])->firstorFail(); 

			$status = 200;

			$content = json_encode([$categ->toArray(), "links "=>["ingredient "=>["href"=>$this->c['router']->pathfor('categ2ingre',['id' => $args['id']])]]]);

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not found : ".$this->c['router']->pathfor('categorie',['id'=>$args['id']])]);

			$this->json_error($resp, $status, $content);
		}
	}
}