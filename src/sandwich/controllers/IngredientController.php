<?php

namespace sandwich\controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \sandwich\models\AppInit as AppInit;

use \sandwich\models\Ingredient;
use \sandwich\models\Categorie;
use \sandwich\models\Commande;
use \sandwich\models\Sandwich;


class IngredientController extends AbstractController{ 

	public function getIngredient(Request $req, Response $resp, $args){
		try{
			$ingred = Ingredient::where('id', '=', $args)->firstorFail();

			$status = 200;

			$content = json_encode(["Ingredient"=>[
													"id"=>$ingred['id'],
													"Nom"=>$ingred['nom'],
													"Categorie"=>[
														"id"=>$ingred['cat_id'],
														"Nom"=>$ingred->CategIngred->nom,
														"Description"=>$ingred->CategIngred->description],
													"Description"=>$ingred['description'],
													"Fournisseur"=>$ingred['fournisseur']]]);

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('ingred', ['id'=>$args['id']])]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function deleteSandwich(Request $req, Response $resp, $args){
		try{
			$commande = Commande::where('token', '=', $args["token"])->firstOrFail();
			$sandwich = Sandwich2Commande::where('id', '=', $args["id"])->firstOrFail();
			$sandwich->delete();
			return $this->json_success($resp, 200, "Le sandwich a bien été supprimé de la commande");
		}
		catch(\Exception $e){
			return $this->json_error($resp, 404, "Sandwich not Found");
		}
	}

	public function deleteIngredient(Request $req, Response $resp, $args){
		try{
			
			$ingred = Ingredient::where('id', '=', $args['id'])->firstOrFail();
			$ingred->delete();

			$status = 200;

			$content = json_encode("Suppression réussie");

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;

			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('deleteIngredient')]);

			$this->json_error($resp, $status, $content);
		}
	}

	public function postIngredient(Request $req, Response $resp, $args){
		try{
			$data = $req->getParsedBody();

			if(empty($data)) {
				return $this->json_error($resp, 400, 'command reation : missing data (all)');
			}

			if (!isset($data['nom'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (nom)');
			}

			if (!isset($data['cat'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (cat)');
			}

			if (!isset($data['description'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (description)');
			}

			if (!isset($data['fournisseur'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (fournisseur)');
			}

			if (!isset($data['image'])) {
				return $this->json_error($resp, 400, 'command reation : missing data (image)');
			}

			$i = new Ingredient;
			$i->nom=$data['nom'];
			$i->cat_id=$data['cat'];
			$i->description=$data['description'];
			$i->fournisseur=$data['fournisseur'];
			$i->img=$data['image'];
			$i->save();

			$status = 200;

			$content = json_encode("Ajout réussie");

			$this->json_success($resp, $status, $content);
		}
		catch(\Exception $e)
		{
			$status = 404;
			$content = json_encode(["error"=> "ressource not allowed : ".$this->c['router']->pathfor('postIngredient')]);

			$this->json_error($resp, $status, $content);
		}
	}
}