<?php

require '../vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \sandwich\models\AppInit as AppInit;

use \sandwich\models\Ingredient;
use \sandwich\models\Categorie;
use \sandwich\models\Commande;
use \sandwich\models\Sandwich;

use \sandwich\controllers\Test;
use \sandwich\controllers\CategorieController;
use \sandwich\controllers\IngredientController;
use \sandwich\controllers\CommandeController;
use \sandwich\controllers\GestionnaireController;


AppInit::bootEloquent('../conf/conf.ini');

$conf=['settings'=>['displayErrorDetails'=>true]];

$app = new \Slim\App(new \Slim\Container($conf));

//accès au détail complet d'une commande
$app->get('/commande/{id}[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
			->getDetailCommande($req, $resp, $args);
	}
)->setName('DetailCommande');

//changement de l'état d'une commande – vérification de la validité de la transition
$app->put('/commande/{id}/etat[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
			->updateEtatCommande($req, $resp, $args);
	}
)->setName('EtatCommande');

//liste complète des commandes , triée par date de livraison et ordre de création
$app->get('/commandes/liste/date[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
			->getDateCommandes($req, $resp, $args);
	}
)->setName('ListeDateCommande');

//liste  des commandes , avec pagination – offset et limit passés dans l'uri
$app->get('/commandes/{offset}/{limit}[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
			->getListeCommandes($req, $resp, $args);
	}
)->setName('ListeCommandes');

//liste  des commandes, avec filtrage sur l'état et sur la date de livraison
$app->post('/commandes/filtrage[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
			->getFiltreCommandes($req, $resp, $args);
	}
)->setName('FiltreCommandes');

$app->get('/connexion/{identifiant}/{password}[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\GestionnaireController($this))
			->getConnexion($req, $resp, $args);
	}
)->setName('Connexion');

$app->delete('/ingredient/{id}[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\IngredientController($this))
			->deleteIngredient($req, $resp, $args);
	}
)->setName('deleteIngredient');

$app->post('/ingredient[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\IngredientController($this))
			->postIngredient($req, $resp, $args);
	}
)->setName('postIngredient');

$app->get('/CA[/]',
	function (Request $req, Response $resp, $args) {
		return (new \sandwich\controllers\CommandeController($this))
			->getCA($req, $resp, $args);
	}
)->setName('CA');

$app->run();