define({ "api": [
  {
    "group": "API_PRIVEE",
    "name": "GetDetailCommande",
    "version": "0.1.0",
    "type": "get",
    "url": "/commande/{id}[/]",
    "title": "Accès au détail complet de la commande",
    "description": "<p>Accès a une ressource de type commande trié par date de livraison et ordre de création</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "date",
            "optional": false,
            "field": "Date",
            "description": "<p>livraison date de livraison de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Float",
            "optional": false,
            "field": "Montant",
            "description": "<p>Montant total de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Array",
            "optional": false,
            "field": "Sandwichs",
            "description": "<p>Tableau de sandwichs avec les détails de chacuns commandés.</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Nom",
            "description": "<p>Le nom des sandwichs qui sont commandés</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Commande",
            "description": "<p>Le nom de la commande</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n\t  {\n\t     \"0\" : {\n\t        \"Date livraison\" : \"2017-02-02 19:40:00\"\n        },\n       \"1\" : {\n\t        \"Montant\" : \"52.700000762939\"\n        },\n       \"2\" : {\n\t        \"Sandwichs\" : \"[\n            \"Nom\" : \"Fromage\",\n            \"Type\" : \"\",\n            \"Prix\" : \"4.50\",\n            \n            \"Nom\" : \"Raclette\",\n            \"Type\" : \"\",\n            \"Prix\" : \"7.80\",\n            \n            \"Nom\" : \"Raclette\",\n            \"Type\" : \"\",\n            \"Prix\" : \"7.80\",\n            \n            \"Nom\" : \"Raclette\",\n            \"Type\" : \"\",\n            \"Prix\" : \"7.80\",\n\n            \"Nom\" : \"Raclette\",\n            \"Type\" : \"\",\n            \"Prix\" : \"3.80\",\n            \n            \"Nom\" : \"Raclette\",\n            \"Type\" : \"\",\n            \"Prix\" : \"3.80\",\n          ]\"\n        },\n        \"Commande\" : {\n\t        \"Nom\" : \"Test\",\n        }\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "API_PRIVEE"
  },
  {
    "group": "API_PRIVEE",
    "name": "GetFiltrage",
    "version": "0.1.0",
    "type": "post",
    "url": "/commandes/filtrage[/]",
    "title": "Liste des commandes avec filtrage",
    "description": "<p>Liste des commandes , avec filtrage sur l'état et sur la date de livraison</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Array",
            "optional": false,
            "field": "Tableau",
            "description": "<p>avec le contenu</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n    {\n\t      \"Commande\" : {\n\t          \"etat\" : \"0\",\n           \"date\" : \"2017-02-20 14:45\",\n       }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 400": [
          {
            "group": "Erreur : 400",
            "optional": false,
            "field": "BadRequest",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 BadRequest\n\n{\n  \"status\": {\n    \"400\": \"command reation : missing data (etat)\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "API_PRIVEE"
  },
  {
    "group": "API_PRIVEE",
    "name": "GetLimit",
    "version": "0.1.0",
    "type": "get",
    "url": "/commandes/{offset}/{limit}[/]",
    "title": "Obtenir la liste des commandes avec pagination",
    "description": "<p>Obtenir la liste complète des commandes avec pagination, offset et limit passées dans l'URL</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Array",
            "optional": false,
            "field": "Tableau",
            "description": "<p>avec le contenu</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n\n[]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 400": [
          {
            "group": "Erreur : 400",
            "optional": false,
            "field": "BadRequest",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 BadRequest\n\n{\n  \"status\": {\n    \"400\": \"command reation : arguments limit cannot be 0\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "API_PRIVEE"
  },
  {
    "group": "API_PRIVEE",
    "name": "GetListeDate",
    "version": "0.1.0",
    "type": "get",
    "url": "/commandes/liste/date[/]",
    "title": "Obtenir la liste des commandes",
    "description": "<p>Obtenir la liste complète des commandes, trièes par date de livraison et date de création</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Id de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "date",
            "optional": false,
            "field": "date_livraison",
            "description": "<p>Date de livraison de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Int",
            "optional": false,
            "field": "etat_avancement",
            "description": "<p>Etat de la commande à un instant précis</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Int",
            "optional": false,
            "field": "etat_paiement",
            "description": "<p>Etat du paiement de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Float",
            "optional": false,
            "field": "montant",
            "description": "<p>Prix total de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>token de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date de Mise à jour de la commmande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date de Création de la commande</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n[\n  {\n\t   \"id\": 8,\n    \"nom\": \"test\",\n    \"date_livraison\": \"2017-02-02 19:40:00\",\n    \"etat_avancement\": 0,\n    \"etat_paiement\": 0,\n    \"montant\": 52.700000762939,\n    \"token\": \"test\",\n    \"updated_at\": \"2017-02-21 15:51:51\",\n    \"created_at\": \"2017-02-21 14:37:49\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "Ressource",
            "description": "<p>NotFound</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 NotFound\n\n{\n  \"status\": {\n    \"404\": \"Ressource not Found\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "API_PRIVEE"
  },
  {
    "group": "API_PRIVEE",
    "name": "PutEtat",
    "version": "0.1.0",
    "type": "put",
    "url": "/commande/{id}/etat[/]",
    "title": "ModifierEtatCommande",
    "description": "<p>On modifie l'état d'une commande</p> <p>Retourne une représentation json de la ressource ajoutée Un format état doit être fourni</p>",
    "parameter": {
      "fields": {
        "Paramètres possible": [
          {
            "group": "Paramètres possible",
            "type": "Int",
            "optional": false,
            "field": "etat",
            "description": "<p>Etat de la commande à modifier</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "  HTTP/1.1 200 OK\n\n{\n \"La commande n°8 a été mise à jour\",\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "  HTTP/1.1 200 OK\n\n{\n \"La commande n°8 ne peut être mise à jour car déjà en cours de livraison\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Commande not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 Not Found\n\n{\n  \"status\": {\n    \"404\": \"Commande not found.\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "API_PRIVEE"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "apiPublic/lbs-apidoc-files/main.js",
    "group": "C__wamp_www_eloquent_sandwichs_apiPublic_lbs_apidoc_files_main_js",
    "groupTitle": "C__wamp_www_eloquent_sandwichs_apiPublic_lbs_apidoc_files_main_js",
    "name": ""
  },
  {
    "group": "Categories",
    "name": "GetAllCategories",
    "version": "0.1.0",
    "type": "get",
    "url": "/categories/",
    "title": "Accès au ressource catégorie",
    "description": "<p>Accès au ressource de type catégorie permet d'accéder à la représentation de toute les ressources categories . Retourne une représentation json de la ressource, incluant son nom et id.</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n\t   {\n\t      \"nb\": 5,\n\t\t  \"categories\": [ \n           {\n             \"id\": 1,\n             \"nom\": \"salades\"\n           },\n           {\n             \"id\": 2,\n             \"nom\": \"crudités\"\n           },\n           {\n             \"id\": 3,\n             \"nom\": \"viandes\"\n           },\n           {\n             \"id\": 4,\n             \"nom\": \"Fromages\"\n           },\n           {\n             \"id\": 5,\n             \"nom\": \"Sauces\"\n           }\n      ]\n   }",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Categories"
  },
  {
    "group": "Categories",
    "name": "GetCategingredients",
    "version": "0.1.0",
    "type": "get",
    "url": "/categories/id/ingredients",
    "title": "Accès au ressource ingrédients d'une catégorie",
    "description": "<p>Accès au ressource de type ingrédients permet d'accéder à la représentation des ressources ingrédients de la categorie désignée. Retourne une représentation json de la ressource, incluant son id, nom, id de la catégorie, sa description, fournisseur et image.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant unique de la catégorie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de l'ingrédient</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de l'ingrédient</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "cat_id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de l'ingrédient</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "fournisseur",
            "description": "<p>Nom du fournisseur de l'ingrédient</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "img",
            "description": "<p>Image de l'ingrédient</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\t\t\n[\n  [\n      {\n           \"id\"  : 1 ,\n           \"nom\" : \"laitue\",\n           \"cat_id\" : 1,\n           \"description\" : \"belle laitue verte\",\n           \"fournisseur\" : \"ferme \\\"la bonne salade\\\"\",\n           \"img\" : null\n       },\n       {\n           ...\n       }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "CategorieNotFound",
            "description": "<p>Categorie inexistante</p>"
          }
        ]
      }
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Categories"
  },
  {
    "group": "Categories",
    "name": "GetCategorie",
    "version": "0.1.0",
    "type": "get",
    "url": "/categories/id",
    "title": "Accès à une ressource catégorie",
    "description": "<p>Accès à une ressource de type catégorie permet d'accéder à la représentation de la ressource categorie désignée. Retourne une représentation json de la ressource, incluant son nom et sa description.</p> <p>Le résultat inclut un lien pour accéder à la liste des ingrédients de cette catégorie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant unique de la catégorie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Link",
            "optional": false,
            "field": "links-ingredients",
            "description": "<p>lien vers la liste d'ingrédients de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n\n{\n   categorie : {\n       \"id\"  : 4 ,\n       \"nom\" : \"Fromages\",\n       \"description\" : \"Nos fromages bios et au lait cru. En majorité des AOC.\"\n   },\n   links : {\n       \"ingredients\" : { \"href\" : \"/eloquent_sandwichs/apiPublic/categorie/4/ingredients }\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "CategorieNotFound",
            "description": "<p>Categorie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas d'erreur",
          "content": "HTTP/1.1 404 Not Found\n\n{\n  \"error\": \"ressource not found : /VMProject/php.iut.local/php.iut.local/www/Project/api/categories/50\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Categories"
  },
  {
    "group": "Categories",
    "name": "GetCategories",
    "version": "0.1.0",
    "type": "get",
    "url": "/categories/id",
    "title": "Accès a une ressource catégorie",
    "description": "<p>Accès a une ressource de type catégorie permet d'accéder à la représentation d'une ressource categorie . Retourne une représentation json de la ressource, incluant son nom et id.</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n\t  {\n\t      \n   }",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Categories"
  },
  {
    "group": "Commandes",
    "name": "GetCommande",
    "version": "0.1.0",
    "type": "get",
    "url": "/commande/{token}/id",
    "title": "Obtenir l'état d'une commande",
    "description": "<p>Obtenir l'état d'une commande avec le détail de celle-ci le nom du sandwich, le type de pain, la taille et le prix</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom du sandwich</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type_pain",
            "description": "<p>le type de pain qui est choisi</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Taille",
            "description": "<p>la taille du pain qui est choisi pour la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Float",
            "optional": false,
            "field": "Prix",
            "description": "<p>Prix du sandwich qui a été choisi</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n\t  {\n     \"Commande\" : {\n       \"Nom\" : \"fromage\",\n       \"Type de pain\" : \"\",\n       \"Taille\" : \"ogre\",\n       \"Prix\" : \"3.40\",\n       }\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 403": [
          {
            "group": "Erreur : 403",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>NoToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 403 Forbidden\n\n{\n  \"status\": {\n    \"403\": \"no token or invalid token\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "GetFacture",
    "version": "0.1.0",
    "type": "get",
    "url": "/commande/{token}/facture",
    "title": "Obtenir la facture d'une commande",
    "description": "<p>Obtenir la facture d'une commande avec le détail de celle-ci la date de livraison, le montant, le sandwich en question et le nom de la commande</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "date",
            "optional": false,
            "field": "Date",
            "description": "<p>livraison Date de livraison pour le sandwich</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Float",
            "optional": false,
            "field": "Montant",
            "description": "<p>Montant de la livraison au total</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Array",
            "optional": false,
            "field": "Sandwichs",
            "description": "<p>Tableau contenant les informations du sandwich commandé</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Nom",
            "description": "<p>Type de fromage dedans</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Type",
            "description": "<p>pain Le type de pain en question</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Taille",
            "description": "<p>La taille du pain qui a été choisi</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Float",
            "optional": false,
            "field": "Prix",
            "description": "<p>Prix du sandwich qui a été choisi</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Commande",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n{\n \"0\": {\n   \"Date livraison :\": \"2017-02-20 20:40:00\"\n },\n \"1\": {\n   \"Montant\" : \"20.5\"\n },\n \"2\": {\n   \"Sandwichs :\": [\n     \"Nom\" : \"fromage\",\n     \"Type Pain\" : \"Pain\", \n     \"Taille\" : \"ogre\",\n     \"Prix\" : \"3.80\",\n   ]\n},\n \"Commande\": {\n   \"Nom :\": \"test1\"\n }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 403": [
          {
            "group": "Erreur : 403",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>NoToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 403 Forbidden\n\n{\n  \"status\": {\n    \"403\": \"no token or invalid token\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "ajouterCommande",
    "version": "0.1.0",
    "type": "put",
    "url": "/commande/{token}/sandwich[/]",
    "title": "Ajouter",
    "description": "<p>On ajoute un sandwich à une commande</p> <p>Retourne une représentation json de la ressource ajoutée Un champ nom, type_pain, taille, ingredients doivent être fournis afin de préparer le sandwich</p>",
    "parameter": {
      "fields": {
        "Paramètres possible": [
          {
            "group": "Paramètres possible",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>nom du sandwich a ajouté</p>"
          },
          {
            "group": "Paramètres possible",
            "type": "String",
            "optional": false,
            "field": "type_pain",
            "description": "<p>le type de pain qui est choisi</p>"
          },
          {
            "group": "Paramètres possible",
            "type": "String",
            "optional": false,
            "field": "taille",
            "description": "<p>la taille du pain pour le client</p>"
          },
          {
            "group": "Paramètres possible",
            "type": "String",
            "optional": false,
            "field": "ingredients",
            "description": "<p>les ingredients dans le sandwich</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "  HTTP/1.1 200 OK\n\n{\n \"Ajout du sandwich\" : [\n     \"Nom Sandwich\" : \"Bavarois\"\n     \"Type de Pain\" : \"Pain\",\n     \"Taille\" : \"Ogre\",\n     \"prix\" : \"25.6\",\n    ]    \t \n {\n     \"Votre Sandwich a été ajouté dans la commande\"\n } \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Commande not found</p>"
          }
        ],
        "Erreur : 400": [
          {
            "group": "Erreur : 400",
            "optional": false,
            "field": "BadRequest",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 Not Found\n\n{\n  \"status\": {\n    \"404\": \"Commande not found.\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 BadRequest\n\n{\n  \"status\": {\n    \"400\": \"Cette taille n'existe pas.\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "deleteCommande",
    "version": "0.1.0",
    "type": "delete",
    "url": "/commande/{token}/supprimer[/]",
    "title": "Supprimer",
    "description": "<p>On supprime une commande</p>",
    "success": {
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "   HTTP/1.1 200 OK\n\n{\n  \"status\": {\n    \"200\": \"La commande a été supprimé avec succès\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "Notfound",
            "description": "<p>Commande not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 Not Found\n\n{\n  \"status\": {\n    \"404\": \"Commande not found.\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "deleteSandwich",
    "version": "0.1.0",
    "type": "delete",
    "url": "/commande/{token}/supprimer/{id}[/]",
    "title": "SuppSand",
    "description": "<p>On supprime un sandwich dans une commande</p>",
    "success": {
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "   HTTP/1.1 200 OK\n\n{\n  \"status\": {\n    \"200\": \"Le sandwich a bien été supprimé de la commande\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "Notfound",
            "description": "<p>Sandwich not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 Not Found\n\n{\n  \"status\": {\n    \"404\": \"Sandwich not found.\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "modifierSandwich",
    "version": "0.1.0",
    "type": "put",
    "url": "/commande/{token}/livraison[/]",
    "title": "ModifierSandwich",
    "description": "<p>On modifie un sandwich</p> <p>Retourne une représentation json de la ressource ajoutée Un format date doit être fournis</p>",
    "parameter": {
      "fields": {
        "Paramètres possible": [
          {
            "group": "Paramètres possible",
            "type": "Float",
            "optional": false,
            "field": "id",
            "description": "<p>Id du sandwich à modifier</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n  {\n   \"Etat du sandwich\" : \n      {\n\t      \"Nom\" : \"Barisien\",\n        \"Montant\" : \"3.60\",\n        \"Type de pain\" : \"Sandwich\"\n      }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Sandwich not found</p>"
          }
        ],
        "Erreur : 403": [
          {
            "group": "Erreur : 403",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>NoToken</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 Not Found\n\n{\n  \"status\": {\n    \"404\": \"Commande not found.\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 403 Forbidden\n\n{\n  \"status\": {\n    \"403\": \"no token or invalid token\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "payerCommande",
    "version": "0.1.0",
    "type": "put",
    "url": "/commande/{token}/paiement[/]",
    "title": "Payer",
    "description": "<p>On paye une commande avec coordonnées bancaires</p> <p>Retourne une représentation json de la ressource ajoutée Un champ nom_prenom, numero_carte, date_expiration, code_verification et doivent être fournis pour payer la commande</p>",
    "parameter": {
      "fields": {
        "Paramètres possible": [
          {
            "group": "Paramètres possible",
            "type": "String",
            "optional": false,
            "field": "nom_prenom",
            "description": "<p>nom et prenom de la personne qui paye</p>"
          },
          {
            "group": "Paramètres possible",
            "type": "Float",
            "optional": false,
            "field": "numero_carte",
            "description": "<p>numero de la carte pour payer</p>"
          },
          {
            "group": "Paramètres possible",
            "type": "date",
            "optional": false,
            "field": "date_expiration",
            "description": "<p>date d'expiration de la carte</p>"
          },
          {
            "group": "Paramètres possible",
            "type": "Float",
            "optional": false,
            "field": "code_verification",
            "description": "<p>code de verification pour payer</p>"
          },
          {
            "group": "Paramètres possible",
            "type": "Float",
            "optional": false,
            "field": "montant",
            "description": "<p>montant de la commande a payer</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "  HTTP/1.1 200 OK\n\n{\n \"La commande a bien été payée\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Commande not found</p>"
          }
        ],
        "Erreur : 400": [
          {
            "group": "Erreur : 400",
            "optional": false,
            "field": "BadRequest",
            "description": "<p>Commande reation</p>"
          }
        ],
        "Erreur : 401": [
          {
            "group": "Erreur : 401",
            "optional": false,
            "field": "NotAllowed",
            "description": "<p>Ressource not allowed</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 NotFound\n\n{\n  \"status\": {\n    \"404\": \"Commande not found.\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 BadRequest\n\n{\n  \"status\": {\n    \"400\": \"Commande reation : missing data (nom_prenom, numero_carte, date_expiration, code_verification, montant).\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 401 NotAllowed\n\n{\n  \"status\": {\n    \"401\": \"Ressource not allowed.\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "postCommandes",
    "version": "0.1.0",
    "type": "post",
    "url": "/commande[/]",
    "title": "Création",
    "description": "<p>On créait une commande</p> <p>Retourne une représentation json de la ressource ajoutée Un label, des coordonnées ainsi qu'un nom doivent être fournis</p>",
    "parameter": {
      "fields": {
        "Paramètres requis": [
          {
            "group": "Paramètres requis",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom du sandwich que l'on veut</p>"
          },
          {
            "group": "Paramètres requis",
            "type": "Float",
            "optional": false,
            "field": "date_livraison",
            "description": "<p>date de livraison du sandwich</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Nom du sandwich créer</p>"
          },
          {
            "group": "Succès : 200",
            "type": "date",
            "optional": false,
            "field": "date_livraison",
            "description": "<p>Date de livraison du sandwich au destinataire</p>"
          },
          {
            "group": "Succès : 200",
            "type": "timezone_type",
            "optional": false,
            "field": "timezone_type",
            "description": "<p>retour du type de la zone</p>"
          },
          {
            "group": "Succès : 200",
            "type": "timezone",
            "optional": false,
            "field": "timezone",
            "description": "<p>retour de l'endroit de la commande</p>"
          },
          {
            "group": "Succès : 200",
            "type": "float",
            "optional": false,
            "field": "Montant",
            "description": "<p>montant de la commande effectuée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Token",
            "description": "<p>token attribué à la commande</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "  HTTP/1.1 200 OK\n\n{\n \"Commande\": {\n   \"name\" : \"panini2\",\n   \"Date de livraison\" : \"2017-02-24 13:34:00\",\n   \"timezone_type\" : \"3\",\n   \"timezone\" : \"Europe/Paris\",\n   \"Montant\" : \"0\",\n   \"Token\" : \"i31c1q9konheqo4tl-e2foflm7ignsw*\"  \n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 400": [
          {
            "group": "Erreur : 400",
            "optional": false,
            "field": "BadRequest",
            "description": ""
          }
        ],
        "Erreur : 401": [
          {
            "group": "Erreur : 401",
            "optional": false,
            "field": "NotAllowed",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 Bad Request\n\n{\n  \"status\": {\n    \"400\": \"Command reation : missing data (all)\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 Bad Request\n\n{\n  \"status\": {\n    \"400\": \"Command reation : missing data (nom)\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 Bad Request\n\n{\n  \"status\": {\n    \"400\": \"Command reation : missing data (date_livraison)\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 400 Bad Request\n\n{\n  \"status\": {\n    \"400\": \"Command reation : invalid argument (date)\"\n  }\n}",
          "type": "json"
        },
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 Not allowed\n\n{\n  \"status\": {\n    \"401\": \"ressource not allowed.\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Commandes",
    "name": "updateCommande",
    "version": "0.1.0",
    "type": "put",
    "url": "/commande/{token}/livraison[/]",
    "title": "Modifier",
    "description": "<p>On modifie une livraison</p> <p>Retourne une représentation json de la ressource ajoutée Un format date doit être fournis</p>",
    "parameter": {
      "fields": {
        "Paramètres possible": [
          {
            "group": "Paramètres possible",
            "type": "date",
            "optional": false,
            "field": "date_livraison",
            "description": "<p>date de la livraison modifié</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Exemple de réponse en cas de succès",
          "content": "  HTTP/1.1 200 OK\n\n{\n \"Votre Commande a bien été mis à jour\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Commande not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 404 Not Found\n\n{\n  \"status\": {\n    \"404\": \"Commande not found.\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Commandes"
  },
  {
    "group": "Ingredients",
    "name": "GetIngredients",
    "version": "0.1.0",
    "type": "get",
    "url": "/ingredients/id",
    "title": "Accès a une ressource ingrédients",
    "description": "<p>Accès a une ressource de type ingrédients permet d'accéder à la représentation d'une ressource ingrédient . Retourne une représentation json de la ressource, incluant son nom et id.</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant de l'ingrédient</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de l'ingrédient</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "fournisseur",
            "description": "<p>Nom du fournisseur de l'ingrédient</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "    HTTP/1.1 200 OK\n\n\t  {\n     \"Ingredient\" : {\n       \"id\" : 2,\n       \"Nom\" : \"roquette\",\n       \"Categorie\" : {\n          \"id\" : 1,\n          \"Nom\" : \"salades\",\n          \"Description\" : \"Nos bonnes salades, fraichement livrées par nos producteurs bios et locaux\"\n       },\n       \"Description\" : \"La roquette qui pète ! Bon, bien sûr et sauvage\",\n       \"Description\" : \"La roquette qui pète ! Bo, bien sûr et sauvage\",\n       \"Fournisseur\" : \"ferme \\\"la bonne salade\\\"\"\n     }\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 405": [
          {
            "group": "Erreur : 405",
            "optional": false,
            "field": "Ressource",
            "description": "<p>not allowed</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple de réponse en cas d'erreur",
          "content": "   HTTP/1.1 405 Bad Ressource not allowed\n\n{\n  \"status\": {\n    \"405\": \"ressource not allowed : /eloquent_sandwichs/apiPublic/ingredient/21\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "apiPublic/run-api.php",
    "groupTitle": "Ingredients"
  }
] });
